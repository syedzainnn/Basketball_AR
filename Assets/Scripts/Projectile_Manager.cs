﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Dweiss;

[ExecuteInEditMode]
[RequireComponent(typeof(LineRenderer), typeof(Rigidbody))]
public class Projectile_Manager : MonoBehaviour
{
	private LineRenderer _lr, _Clr;
	private Rigidbody _rb,_crb;

	public float timeBeteenStep = 1;
	public int stepCount = 55;

	public Vector3 addedV, addedF;

	private Vector3 InitialTouchPosition, FinalTouchPosition,AppliedForce;
	private float InitialTouchTime, FinalTouchTime,xAxisForce,yAxisForce,zAxisForce;
	private float deltaZ = 0.0f; private float deltaX = 0.0f; private float deltaY = 0.0f;
	private Rigidbody rb,crb;
	private Vector3 gravityy = new Vector3(0,-12,0);
	Color Red = Color.red;
	Color red = Color.red;

	private Vector3 deltaPositioN,appliedTraj;

	public Vector3 PresentLocation = new Vector3( 0,33,531);
	public GameObject Physics_present; 
	private GameObject Present_Clone; 
	public GameObject Visual_present;

	private bool isThrown;
	private bool right = true;
	private int availableShots;
	public GameObject GameoverGUI;
	public  Text RemainingShots;
	private Renderer[] rend;
	public GameObject boy;
	private float boySpeed = 0.5f;

	void Start()
	{
		RemainingShots.text = "10";
		GameoverGUI.SetActive(false);
		availableShots = 10;
		isThrown = false;
		Time.timeScale = 3f;
		Debug.Log ("Mouse Click to Shoot Aim And Shoot");
		Renderer[] rend = Visual_present.GetComponentsInChildren<Renderer> ();
		foreach (Renderer rn in rend) {
			rn.enabled = true;
		}
	}

	private void AddPower()
	{
		isThrown = true;
		_crb = Present_Clone.GetComponent<Rigidbody> ();
		CalcTime();
		_crb.useGravity = true;
		_crb.velocity += AppliedForce;
		AppliedForce = Vector3.zero;
		_crb.AddForce (addedF, ForceMode.Force);
		addedF = Vector3.zero;



		//_rb.velocity += addedV;

		//_rb.useGravity=true;
		//_rb.velocity += AppliedForce;
		//addedV = Vector3.zero;
		//AppliedForce = Vector3.zero;

		//_rb.AddForce(addedF, ForceMode.Force);
		//addedF = Vector3.zero;


	}


	private void CalcTime()
	{
		Vector3[] t = _crb.CalculateTime(new Vector3(0, 0, 0),
			addedV, addedF);

//		Vector3[] t = _rb.CalculateTime(new Vector3(0, 0, 0),
//			addedV, addedF);
		var timeT = new Vector3[]{
			new Vector3(Time.time + t[0].x, Time.time + t[0].y, Time.time + t[0].z),
			new Vector3(Time.time + t[1].x, Time.time + t[1].y, Time.time + t[1].z)
		};
	}





	private void DrawMovementLine()
	{
		_crb = Present_Clone.GetComponent<Rigidbody> ();
		_Clr = Present_Clone.GetComponent<LineRenderer> ();


		var res = _crb.CalculateMovement (stepCount, timeBeteenStep, AppliedForce, addedF); 
		//var res = _rb.CalculateMovement(stepCount, timeBeteenStep, AppliedForce, addedF);

	
		_Clr.positionCount = stepCount + 1;
		_Clr.SetPosition (0, transform.position);
		_Clr.material = new Material (Shader.Find ("Particles/Additive"));
		_Clr.SetColors (Red, red);
		_Clr.enabled=true; 
		for (int i = 0; i < res.Length; i++) {
			_Clr.SetPosition (i + 1, res [i]);
		}

	

//		_lr.positionCount = stepCount + 1;
//		_lr.SetPosition(0, transform.position);
//		_lr.material= new Material(Shader.Find("Particles/Additive"));
//		_lr.SetColors (Red, red);
//		for (int i = 0; i < res.Length; ++i)
//		{
//			_lr.SetPosition(i+1, res[i]);
//		}
		

	
	}

	public void restart()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
	void Update () 

	{
		if (Input.GetMouseButton (0) && availableShots >0) 
		{
			deltaX -= Input.GetAxis ("Mouse X");
			deltaY += Input.GetAxis ("Mouse Y");
			deltaZ -= Time.deltaTime * 4f;
			//Debug.Log ("Delta X" + deltaX);
			Debug.Log ("Delta Y" + deltaY);
			//Debug.Log ("DeltaZ" + deltaZ);
			AppliedForce = new Vector3 (deltaX, deltaY*4f +10f, deltaZ*3f-10f);
			if (Present_Clone == null && availableShots != 0) {
				Present_Clone = Instantiate (Physics_present, PresentLocation, Quaternion.Euler (-90, 0, 0)) as GameObject; 
				//_Clr = Present_Clone.GetComponent<LineRenderer> ();
				//_crb = Present_Clone.GetComponent<Rigidbody> ();
				}
			DrawMovementLine (); 
			 
		}
	
		else 
		{
			if (isThrown) 
			{
				//_Clr = Present_Clone.GetComponent<LineRenderer> ();
				if(_Clr !=null)
				_Clr.enabled = false;

				if(Present_Clone.transform.position.y < -30f || Present_Clone.transform.position.y>250f || Present_Clone.transform.position.z<100f || Present_Clone.transform.position.x < -188f || Present_Clone.transform.position.x>188f) 
				{
					isThrown = !isThrown;
					if(Present_Clone != null)
					Destroy (Present_Clone);
					UpdateScore ();
				}
			}
			deltaX = 0.0f;
			deltaZ = 0.0f;
			deltaY = 0.0f;
		}


		if(Input.GetMouseButtonUp(0))
		{
			AddPower();
			Debug.Log (availableShots);
		}
		if (availableShots == 0) 
		{
			Time.timeScale = 0;
			GameoverGUI.SetActive (true);
			Renderer[] rend = Visual_present.GetComponentsInChildren<Renderer> ();
			foreach (Renderer rn in rend) {
				rn.enabled = false;
			}

		}
	}


	public void  UpdateScore () 
	{
		availableShots--;
		RemainingShots.text = availableShots.ToString();
	}
	void FixedUpdate ()
	{
		if (availableShots <= 5) 
		{

			if (boy.transform.position.x <121f && right) 
			{
				boy.transform.position += new Vector3 (boySpeed, 0, 0);
				}
			if (boy.transform.position.x >= 121f) {
						right = false;
				}
				if (right == false) 
			{
				boy.transform.position -= new Vector3 (boySpeed, 0, 0);
				}
			if (boy.transform.position.x <= -115f) 
			{
						right = true;
				}
		}


		else {
			boy.transform.position += new Vector3 (0, 0, 0);
		}
	
	}


}
