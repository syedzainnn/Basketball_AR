﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TouchManager : MonoBehaviour 
{
	private float initialTouchTime;
	private float finalTouchTime;
	private Vector2 initialTouchPosition;
	private Vector2 FinalTouchPosition;
	private float xAxisForce;
	private float yAxisForce;
	private float zAxisForce;
	private Vector3 requiredForce;
	private Rigidbody rb,crb;
	private GameObject ballClone;
	private bool thrown = false;
	public static int availableShots;
	//private bool right = true;
	//private float arrowSpeed = 1.5f;
	public static int i=0;
	private Vector3 ballpos = new Vector3(0,0,0);
	//Renderer rend;
	Renderer [] rend;

	public GameObject Ball_;
	public GameObject VisualBall;
	public GameObject staticParticle;
	public Text availableShotsGO;
	public GameObject GameoverUI;
	public GameObject TrailsParticles;
	public Vector3 ballPos;
	public GameObject arrow;	

		
	void Start () 
	{
		
		availableShots = 5;
		availableShotsGO.text = "5";
		GameoverUI.SetActive(false);
		rb = Ball_.GetComponent<Rigidbody> ();

		Renderer[] rend = VisualBall.GetComponentsInChildren<Renderer> ();
		foreach (Renderer r in rend) 
		{
			r.enabled = true;
		}
		//rend =VisualBall.GetComponent<Renderer> ();
		//rend.enabled = true;
		rb.useGravity = false;
		Time.timeScale =3.5f;
		staticParticle.SetActive (true);
		TrailsParticles.SetActive (false);
		Physics.gravity = new Vector3 (0, -33, 0);
	}
		
	void FixedUpdate () 
	{
				
//				if (availableShots > 0) {
//			
//						if (arrow.transform.localPosition.x < -300f && right) {
//								arrow.transform.localPosition += new Vector3 (arrowSpeed, 0, 0);
//						}
//						if (arrow.transform.localPosition.x >= -300f) {
//								right = false;
//						}
//						if (right == false) {
//								arrow.transform.localPosition -= new Vector3 (arrowSpeed, 0, 0);
//						}
//						if (arrow.transform.localPosition.x <= -530f) {
//								right = true;
//						}
//				}
//
//
//				else {
//						arrow.transform.localPosition += new Vector3 (0, 0, 0);
//				}



	


			if (ballClone != null && ballClone.transform.position.y < -61f) 
			{
				OnDestroy ();
			}
		if (i == 0 && availableShots ==0 ) 
		{
			GameOver ();
		}	
						//				if (ballClone !=null&&  ballClone.transform.position.y < -120f) {
						//						
						//								Destroy (ballClone);
						//
						//						thrown= false;
				
		OnParticlesFollow ();




//				if (ballClone != null) {
//						ballpos = GameObject.FindGameObjectWithTag ("Clone").transform.position;
//						//Debug.Log (ballpos);
//						TrailsParticles.SetActive (true);
//						TrailsParticles.transform.position = Vector3.Lerp(TrailsParticles.transform.position, ballpos,0.5f);
//						//Debug.Log (ballpos);
//				}
				////
				////				if (ball.transform.position.y < -120f) {
				////						Destroy (ball);
				////				}
				//				}






		}
		private void OnParticlesFollow()
		{
			if (ballClone != null) 
			{
				ballpos = GameObject.FindGameObjectWithTag ("Clone").transform.position;
				TrailsParticles.SetActive (true);
				TrailsParticles.transform.position = Vector3.Lerp(TrailsParticles.transform.position, ballpos,1f);

			}
		}



	public void OnDestroy() 
		{
			GameObject [] clones = GameObject.FindGameObjectsWithTag("Clone");
			foreach (GameObject a in clones) 
			{
				Destroy (a);
				i--;
				thrown = false;
			}
		}

		private void Ballthrow ()
		{
			float intensity =(1/Mathf.Abs( arrow.transform.localPosition.x ))*10000f;
		ballClone = Instantiate (Ball_, ballPos, Quaternion.Euler(-90,0,0)) as GameObject;
		//	ballClone = Instantiate (Ball_, VisualBall.transform.position, transform.rotation) as GameObject;
			i++;
			ballClone.tag = "Clone";
			crb = ballClone.GetComponent<Rigidbody> ();
			xAxisForce = FinalTouchPosition.x - initialTouchPosition.x ;
			yAxisForce = FinalTouchPosition.y - initialTouchPosition.y;
			zAxisForce = finalTouchTime - initialTouchTime;
			requiredForce = new Vector3 (-xAxisForce/6f, yAxisForce/9f +intensity, -(zAxisForce*40f+intensity));
			crb.useGravity = true;
			crb.velocity = requiredForce;
		}


		
	public void onTouchDown () 
	{
		initialTouchTime = Time.time;
		initialTouchPosition = Input.mousePosition;
	}
		
	public void onTouchUp ()
	{
		finalTouchTime = Time.time;
		FinalTouchPosition = Input.mousePosition;
		if(Input.GetMouseButtonUp(0) && !thrown && availableShots>0) // && ballClone == null)
		{
			Ballthrow ();
			availableShots--;
			availableShotsGO.text = availableShots.ToString();

		}
		if (availableShots == 0)
		{
			Renderer[] rend = VisualBall.GetComponentsInChildren<Renderer> ();
			foreach (Renderer r in rend) 
			{
				r.enabled = false;
			}
			//Renderer rend = VisualBall.GetComponent<Renderer> ();
			//rend.enabled = false;
			staticParticle.SetActive (false);
		}
	}
		

	public void restart()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}


	IEnumerator Delay( float wait)
	{
		yield return new WaitForSeconds (wait);
	}

	public void GameOver() 
	{
		GameoverUI.SetActive (true);
	}





}