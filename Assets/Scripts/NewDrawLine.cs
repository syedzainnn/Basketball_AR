﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Dweiss;

[ExecuteInEditMode]
[RequireComponent(typeof(LineRenderer), typeof(Rigidbody))]
public class NewDrawLine : MonoBehaviour
{
	private LineRenderer _lr;
	private Rigidbody _rb;

	public float timeBeteenStep = 1;
	public int stepCount = 55;

	public Vector3 addedV, addedF;

	private Vector3 InitialTouchPosition, FinalTouchPosition,AppliedForce;
	private float InitialTouchTime, FinalTouchTime,xAxisForce,yAxisForce,zAxisForce;
	private float deltaZ = 0.0f; private float deltaX = 0.0f; private float deltaY = 0.0f;
	private Rigidbody rb;
	private Vector3 gravityy = new Vector3(0,-12,0);
	Color Red = Color.red;
	Color red = Color.red;

	private Vector3 deltaPositioN,appliedTraj;





	void Start()
	{
		//Time.timeScale = 2f;
		_lr = GetComponent<LineRenderer>();
		_rb = GetComponent<Rigidbody>();
		Debug.Log ("Mouse Click to Aim and Shoot");
		_lr.enabled = false;
	}

	private void AddPower()
	{
		Debug.LogWarning("Change V: " + addedV + " F: " + addedF);
		CalcTime();

		//_rb.velocity += addedV;
		_rb.useGravity=true;
		_rb.velocity += AppliedForce;
		//addedV = Vector3.zero;
		AppliedForce = Vector3.zero;

		_rb.AddForce(addedF, ForceMode.Force);
		addedF = Vector3.zero;


	}


	private void CalcTime()
	{

		Vector3[] t = _rb.CalculateTime(new Vector3(0, 0, 0),
			addedV, addedF);

		var timeT = new Vector3[]{
			new Vector3(Time.time + t[0].x, Time.time + t[0].y, Time.time + t[0].z),
			new Vector3(Time.time + t[1].x, Time.time + t[1].y, Time.time + t[1].z)
		};
		Debug.LogWarning(Time.time + ": assuming no drag touch in (0,0,0) occures in those 2 time stamps:  " + timeT[0] + ", " + timeT[1]);
	}

	private void DrawMovementLine()
	{
		

		var res = _rb.CalculateMovement(stepCount, timeBeteenStep, AppliedForce, addedF);
		//var res = _rb.CalculateMovement(stepCount, timeBeteenStep, appliedTraj, appliedTraj);

		_lr.positionCount = stepCount + 1;
		_lr.SetPosition(0, transform.position);
		_lr.material= new Material(Shader.Find("Particles/Additive"));
		_lr.SetColors (Red, red);

		for (int i = 0; i < res.Length; ++i)
		{
			_lr.SetPosition(i+1, res[i]);
		}

	}
	void Update () 

	{
		if (Input.GetMouseButton (0)) 
		{
			_lr.enabled = true;
			deltaZ -= Time.deltaTime *4f;
			deltaX -= Input.GetAxis ("Mouse X") ;
			deltaY += Input.GetAxis ("Mouse Y");
			Debug.Log ("Delta X" + deltaX);
			Debug.Log ("Delta Y" + deltaY);
			Debug.Log ("DeltaZ" + deltaZ);
			AppliedForce = new Vector3 (deltaX, deltaY*3f,deltaZ*3f);
			// appliedTraj = new Vector3 (deltaX, deltaY *5f, deltaZ *5f);
		} 

		else 
		{
			_lr.enabled = false;
			deltaX = 0.0f;
			deltaZ = 0.0f;
			deltaY = 0.0f;
		}
	DrawMovementLine();

		if(Input.GetMouseButtonUp(0))
		{
			AddPower();
		}
	
	}

	//	public void onTouchDown() 
	//	{
	//		InitialTouchPosition = Input.mousePosition;
	//		InitialTouchTime = Time.time;
	//		Debug.Log("Initial Touch Position" + InitialTouchPosition);
	//
	//		xAxisForce = FinalTouchPosition.x - InitialTouchPosition.x;
	//		yAxisForce = FinalTouchPosition.y - InitialTouchPosition.y;
	//		zAxisForce = FinalTouchTime - InitialTouchTime;
	//		AppliedForce = new Vector3 (xAxisForce, yAxisForce, zAxisForce);
	//		Debug.Log (xAxisForce + yAxisForce + zAxisForce);
	//	}
	//	public void onTouchUp () {
	//
	//		FinalTouchPosition = Input.mousePosition;
	//		FinalTouchTime = Time.time;
	//		Debug.Log ("Final Touch Position:" + FinalTouchPosition);
	//		//Debug.Log("difference between" + FinalTouchPosition - InitialTouchPosition );

}
